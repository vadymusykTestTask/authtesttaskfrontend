import {Component, OnInit} from "@angular/core";
import {HTTPService} from "../services/http.service";
import {EventService} from "../services/event.service";

@Component({
    selector: 'nav-tab',
    templateUrl: './app/nav.component/nav.component.html',
    styleUrls: ['./app/nav.component/nav.component.css'],
})

export class NavigationComponent implements OnInit {
    panels: any;
    firstPanel: string[] = [];
    firstPanelNumber: number = 0;
    clickedButtonFirstPanel: number = 0;

    constructor(private _http: HTTPService, private _evtSer: EventService) {
        this.firstPanelNumber = 0;
    }

    ngOnInit(): void {
        this._http.getJson('resources/navProperties.json').subscribe(buttons => {
            this.panels = buttons;
            this.firstPanel = this.panels.firstPanel;
        });
    }

    selectPage(index: number) {
        this._evtSer.selectPage(index);
        this.firstPanelNumber = index;
        this.clickedButtonFirstPanel = index;
    }

}
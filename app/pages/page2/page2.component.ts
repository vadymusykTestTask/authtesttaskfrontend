
import {Component} from "@angular/core";
import {EventService} from "../../services/event.service";
import {HTTPService} from "../../services/http.service";
import {SecurityService} from "../../services/authorization.service";
import {Content} from "../../entity/Content";
@Component(
    {
        selector: 'page2',
        moduleId: module.id,
        templateUrl: 'page2.component.html',
        styleUrls: ['page2.component.css']
    }
)
export class Page2Component{
    userName: string  = '';
    role: string = '';
    isActive: boolean = false;
    pageContent: string = '';
    noAccess: boolean = true;
    contentId: number = 0;

    constructor(private _evt: EventService, private _http: HTTPService, private _sec: SecurityService) {
        this.userName =this._sec.getLogin();
        this.isActive = false;
        this.initData();
    }

    initData(): void{
        this._http.getJson('http://localhost:9191/uaa/user').subscribe(data=>{
            this.role = data.authorities[0].authority;
            this.getAccessForEdit(this.role);
        });

        this._http.getJson('http://localhost:9191/uaa/content/2').subscribe(data =>{
            this.pageContent = data.content;
            this.contentId = data.id;

        });
    }

    edit() {
        console.log(this.pageContent);
        this._http.put('http://localhost:9191/uaa/content', new Content(this.contentId, 2, this.pageContent))
            .subscribe(() =>{
                this.initData();
            });
    }

    getAccessForEdit(role: string): void {
        console.log(this.role);
        switch (role) {

            case 'ADMIN':
                this.noAccess = false;
                this.isActive = true;
                break;
            case "MODERATOR":
                this.noAccess = false;
                this.isActive = true;
                break;
            case "USER":
                this.noAccess = true;
                this.isActive = false;
                break;
            default:
                this.noAccess = true;
                this.isActive = false;
                break;
        }
    }

    logOut(): void {
        this._evt.showAuthorization(true);
        this._evt.selectPage(0);
    }
}
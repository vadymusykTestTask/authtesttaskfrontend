import {Component} from "@angular/core";
import {SecurityService} from "../../services/authorization.service";
import {HTTPService} from "../../services/http.service";
import {EventService} from "../../services/event.service";
import {Content} from "../../entity/Content";
@Component(
    {
        selector: 'page3',
        moduleId: module.id,
        templateUrl: 'page3.component.html',
        styleUrls: ['page3.component.css']
    }
)
export class Page3Component{
    userName: string  = '';
    role: string = '';
    isActive: boolean = false;
    pageContent: string = '';
    noAccess: boolean = true;
    isVisible: boolean = true;
    contentId: number = 0;

    constructor(private _evt: EventService, private _http: HTTPService, private _sec: SecurityService) {
        this.userName =this._sec.getLogin();
        this.isActive = false;
        this.initData();

    }

    initData(): void{
        this._http.getJson('http://localhost:9191/uaa/user').subscribe(data=>{
            this.role = data.authorities[0].authority;
            this.getAccessForEdit(this.role);
        });

        this._http.getJson('http://localhost:9191/uaa/content/3').subscribe(data =>{
            this.pageContent = data.content;
            this.contentId = data.id;

        });
    }

    edit() {
        console.log(this.pageContent);
        this._http.put('http://localhost:9191/uaa/content', new Content(this.contentId, 2, this.pageContent))
            .subscribe(() =>{
                this.initData();
            });
    }

    getAccessForEdit(role: string): void {
        console.log(this.role);
        switch (role) {

            case 'ADMIN':
                this.noAccess = false;
                this.isVisible = true;
                this.isActive = true;
                break;
            case "MODERATOR":
                this.noAccess = true;
                this.isVisible = true;
                this.isActive = false;
                break;
            case "USER":
                this.noAccess = true;
                this.isVisible = false;
                this.isActive = false;
                break;
            default:
                this.noAccess = true;
                this.isVisible = true;
                this.isActive = false;
                break;
        }
    }


    logOut(): void {
        this._evt.showAuthorization(true);
        this._evt.selectPage(0);
    }
}
import {Component} from "@angular/core";
import {EventService} from "../../services/event.service";
import {HTTPService} from "../../services/http.service";
import {SecurityService} from "../../services/authorization.service";
import {Content} from "../../entity/Content";
@Component(
    {
        selector: 'page1',
        moduleId: module.id,
        templateUrl: 'page1.component.html',
        styleUrls: ['page1.component.css']
    }
)
export class Page1Component {
    userName: string = '';
    role: string = '';
    isActive: boolean = false;
    noAccess: boolean = true;
    pageContent: string = '';
    contentId: number = 0;

    constructor(private _evt: EventService, private _http: HTTPService, private _sec: SecurityService) {
        this.userName = this._sec.getLogin();
        this.isActive = false;
        this.initData();
    }

    initData(): void {
        this._http.getJson('http://localhost:9191/uaa/user').subscribe(data => {
            this.role = data.authorities[0].authority;
            this.getAccessForEdit(this.role);
        });

        this._http.getJson('http://localhost:9191/uaa/content/1').subscribe(data => {
            this.pageContent = data.content;
            this.contentId = data.id;
        });

    }

    edit() {
        console.log(this.pageContent);
        this._http.put('http://localhost:9191/uaa/content', new Content(this.contentId, 1, this.pageContent))
            .subscribe(() =>{
            this.initData();
            });
    }

    getAccessForEdit(role: string): void {
        switch (role) {

            case 'ADMIN':
                this.noAccess = false;
                this.isActive = true;
                break;
            case "MODERATOR":
                this.noAccess = true;
                this.isActive = false;
                break;
            case "USER":
                this.noAccess = true;
                this.isActive = false;
                break;
            default:
                this.noAccess = true;
                this.isActive = false;
                break;
        }
    }

    logOut(): void {
        this._evt.showAuthorization(true);
        this._evt.selectPage(0);
    }
}
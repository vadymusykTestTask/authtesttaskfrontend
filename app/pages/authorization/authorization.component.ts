import {Component} from "@angular/core";
import {HTTPService} from "../../services/http.service";
import {EventService} from "../../services/event.service";
import {SecurityService} from "../../services/authorization.service";

@Component({
    selector: 'authorization',
    moduleId: module.id,
    templateUrl: 'authorization.component.html',
    styleUrls: ['authorization.component.css']
})

export class AuthorizationComponent {
    login: string = null;
    password: any = null;
    isShowErrorMessage: boolean = false;
    errorMessage: string = null;
    errorCode:string = null;
    isLoginWrong: boolean = false;
    isPasswordWrong: boolean = false;

    constructor(private _http: HTTPService, private _event: EventService, private _authorization:SecurityService) {
        this.errorMessage = '';
        this.isShowErrorMessage = false;
    }

    loginChanged() {
        this.isShowErrorMessage = false;
        this.isLoginWrong = false;
        this.isPasswordWrong = false;
    }

    passwordChanged(event) {
        this.isShowErrorMessage = false;
        this.isLoginWrong = false;
        this.isPasswordWrong = false;
        if (event.code == 'Enter') {
            this.logIn();
        }
    }

    logIn() {
        this._http.logIn('http://localhost:9191/uaa/oauth/token', this.login, this.password).subscribe(res => {
            this._authorization.setToken(res.access_token);
            this._authorization.setLogin(this.login);
            this._event.showMainContent(true);
            this.isShowErrorMessage = false;
        }, error => {
            this.isShowErrorMessage = true;
            this.errorCode = error.status;
            this.errorMessage = 'Пожалуйста, введите корректный логин и пароль.';
            console.error('Код ошибки:', error.status);
            console.error('Ошибка:', error.json().error);
            console.error('Описание ошибки:', error.json().error_description);
            console.log('Полный код ошибки:', error);
        });
    }
}
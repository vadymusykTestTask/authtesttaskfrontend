import {Component} from "@angular/core";
import {HTTPService} from "../../services/http.service";
import {EventService} from "../../services/event.service";
import {SecurityService} from "../../services/authorization.service";
declare let $: any;
@Component(
    {
        selector: 'home-page',
        moduleId: module.id,
        templateUrl: 'home.component.html',
        styleUrls: ['home.component.css']
    }
)
export class HomeComponent {

    userName: string  = '';
    role: string = '';
    isActive: boolean = false;
    pageContent: string = '';

    constructor(private _evt: EventService, private _http: HTTPService, private _sec: SecurityService) {
        this.clearData();
        this.userName =this._sec.getLogin();
        this.initData();
        this.isActive = false;
    }

    initData(): void{
        this._http.getJson('http://localhost:9191/uaa/user').subscribe(data=>{
            this.role = data.authorities[0].authority;
        });

        this._http.getJson('http://localhost:9191/uaa/content/0').subscribe(data =>{
            this.pageContent = data.content;

        });
    }

    isq(): void {
        this._evt.isShowAuthorization.subscribe(data => {
            console.log("Data: " + data);
        });
    }

    logOut(): void {
        this._evt.showAuthorization(true);
    }

    clearData(): void{
        this.initData();
    }

}
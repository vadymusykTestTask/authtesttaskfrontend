import {Component} from "@angular/core";
import {EventService} from "./services/event.service";
declare let $: any;
@Component({
    selector: 'front-end-part',
    templateUrl: './app/app.component.html'
})
export class FrontEndPart {
    selectedPage: number = 0;
    isShowMainContent: boolean = false;
    isShowAuthorizationWindow: boolean = true;

    constructor(private _evtSer: EventService) {
        this.selectedPage = 0;
        _evtSer.selectCurrentPage.subscribe(data => {
            this.selectedPage = data;
        });
        _evtSer.isShowAuthorization.subscribe((showAuthorize: boolean) => {
            this.isShowAuthorizationWindow = showAuthorize;
            this.isShowMainContent = !showAuthorize;
        });
        _evtSer.isShowMainContent.subscribe((isAuthorize: boolean) => {
            this.isShowMainContent = isAuthorize;
            this.isShowAuthorizationWindow = !isAuthorize;
        });
    }
}

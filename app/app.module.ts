import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {FrontEndPart} from "./app.component";
import {AuthorizationComponent} from "./pages/authorization/authorization.component";
import {FormsModule} from "@angular/forms";
import {HTTPService} from "./services/http.service";
import {EventService} from "./services/event.service";
import {SecurityService} from "./services/authorization.service";
import {NavigationComponent} from "./nav.component/nav.component";
import {Page1Component} from "./pages/page1/page1.component";
import {Page2Component} from "./pages/page2/page2.component";
import {Page3Component} from "./pages/page3/page3.component";
import {HomeComponent} from "./pages/home/home.component";

@NgModule({
    imports: [BrowserModule, HttpModule, FormsModule],
    declarations: [
        FrontEndPart,
        AuthorizationComponent,
        NavigationComponent,
        Page1Component,
        Page2Component,
        Page3Component,
        HomeComponent
    ],
    bootstrap: [FrontEndPart],
    providers: [HTTPService, EventService, SecurityService]
})
export class AppModule {

}

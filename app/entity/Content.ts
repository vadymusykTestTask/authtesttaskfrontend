export class Content{
    id: number;
    page: number;
    content: string;


    constructor(id: number, page: number, content: string) {
        this.id = id;
        this.page = page;
        this.content = content;
    }
}
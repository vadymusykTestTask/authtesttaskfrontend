import {Injectable, EventEmitter} from "@angular/core";


@Injectable()
export class EventService {
    isShowMainContent: EventEmitter<boolean>;
    selectCurrentPage: EventEmitter<any>;
    isShowAuthorization: EventEmitter<boolean>;



    constructor() {
        this.isShowMainContent = new EventEmitter();
        this.selectCurrentPage = new EventEmitter();
        this.isShowAuthorization = new EventEmitter();
    }

    showMainContent(isShow: boolean) {
        this.isShowMainContent.emit(isShow);
    }


    selectPage(index: any) {
        this.selectCurrentPage.emit(index);
    }


    showAuthorization(isShow: boolean){
        this.isShowAuthorization.emit(isShow);
    }

}

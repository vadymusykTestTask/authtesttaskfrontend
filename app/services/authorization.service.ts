import {Injectable} from "@angular/core";

@Injectable()

export class SecurityService {
    private token: string = '';

    private login: string = '';

    public setToken(token: string) {
        this.token = token;
    }

    public getToken(): string {
        return this.token;
    }

    public setLogin(login: string) {
        this.login = login;
    }

    public getLogin(): string {
        return this.login;
    }
}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var authorization_service_1 = require("./authorization.service");
var HTTPService = (function () {
    function HTTPService(_http, _authorization) {
        this._http = _http;
        this._authorization = _authorization;
    }
    HTTPService.prototype.getJson = function (url) {
        var headers = new http_1.Headers();
        this.createAuthorizationHeader(headers);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.get(url, options).map(function (response) { return response.json(); });
    };
    HTTPService.prototype.createAuthorizationHeader = function (headers) {
        headers.append('Authorization', 'Bearer ' + this._authorization.getToken());
    };
    HTTPService.prototype.postEntity = function (url, content) {
        var body = JSON.stringify(content);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.createAuthorizationHeader(headers);
        var options = new http_1.RequestOptions({ headers: headers, method: 'post' });
        return this._http.post(url, body, options);
    };
    HTTPService.prototype.postEntityWithExistentBody = function (url, content) {
        var body = content;
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.createAuthorizationHeader(headers);
        var options = new http_1.RequestOptions({ headers: headers, method: 'post' });
        return this._http.post(url, body, options);
    };
    HTTPService.prototype.put = function (url, content) {
        var body = JSON.stringify(content);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.createAuthorizationHeader(headers);
        var options = new http_1.RequestOptions({ headers: headers, method: 'put' });
        return this._http.put(url, body, options);
    };
    HTTPService.prototype.logIn = function (url, login, password) {
        var body = "?username=" + login + "&password=" + password +
            "&grant_type=password&scope=openid&client_secret=acmesecret&client_id=acme";
        var headers = new http_1.Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Basic YWNtZTphY21lc2VjcmV0'
        });
        var options = new http_1.RequestOptions({ headers: headers, method: 'post' });
        return this._http.post(url + body, [], options).map(function (response) {
            return response.json();
        });
    };
    return HTTPService;
}());
HTTPService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, authorization_service_1.SecurityService])
], HTTPService);
exports.HTTPService = HTTPService;
//# sourceMappingURL=http.service.js.map
import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";
import "rxjs/add/operator/map";
import {SecurityService} from "./authorization.service";
declare let $: any;
@Injectable()
export class HTTPService {
    constructor(private _http: Http, private _authorization: SecurityService) {

    }

    getJson(url) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);
        let options = new RequestOptions({headers: headers});
        return this._http.get(url, options).map(response => response.json());
    }

    createAuthorizationHeader(headers: Headers) {
        headers.append('Authorization', 'Bearer ' + this._authorization.getToken());
    }

    postEntity(url: string, content: any) {
        let body = JSON.stringify(content);
        let headers = new Headers({'Content-Type': 'application/json'});
        this.createAuthorizationHeader(headers);
        let options = new RequestOptions({headers: headers, method: 'post'});
        return this._http.post(url, body, options);
    }

    postEntityWithExistentBody(url: string, content: any) {
        let body = content;
        let headers = new Headers({'Content-Type': 'application/json'});
        this.createAuthorizationHeader(headers);
        let options = new RequestOptions({headers: headers, method: 'post'});
        return this._http.post(url, body, options);
    }


    put(url: string, content: any) {
        let body = JSON.stringify(content);
        let headers = new Headers({'Content-Type': 'application/json'});
        this.createAuthorizationHeader(headers);
        let options = new RequestOptions({headers: headers, method: 'put'});
        return this._http.put(url, body, options);
    }

    logIn(url: string, login: string, password: string) {
        let body: any = "?username=" + login + "&password=" + password +
            "&grant_type=password&scope=openid&client_secret=acmesecret&client_id=acme";
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Basic YWNtZTphY21lc2VjcmV0'
        });
        let options = new RequestOptions({headers: headers, method: 'post'});
        return this._http.post(url + body, [], options).map(response =>
            response.json());
    }


}